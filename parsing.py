from bs4 import BeautifulSoup

#funcao que busca no em uma string html os campos src das tags img
def get_src_img(html):
    
    #e utilizado uma biblioteca chamada beautiful soup para fazer o parsing
    #cria o objeto de parsing do html
    soup = BeautifulSoup(html, 'html.parser')
    #vetor que ira guardar os caminhos das imagens
    img_src_urls = []
    #para cada tag img no html
    for img in soup.find_all('img'):
        #adiciona no vetor o campo src dessa tag
        img_src_urls.append(img.get('src'))

    #retorna o vetor de caminhos
    return img_src_urls

#funcao que trata a url da entrada do usuario e retorna o host
def tratar_url(url):

    #separa a url em antes e depois do // do http://
    nova_url = url.split("//")
    #se havia algum // na url
    if len(nova_url) > 1:
        #pega o que havia depois do //
        nova_url = nova_url[1]
    else:
        #se nao pega a url de volta 
        nova_url = nova_url[0]
    
    #separa a url na primeira /, separando o host do caminho
    nova_url = nova_url.split("/", 1)
    #se tinha algum caminho na url
    if len(nova_url) > 1:
        #retorna o host alvo e o caminho
        return nova_url[0], nova_url[1]
    else:
        #se nao tinha retorna o host algo e none
        return nova_url[0], None

#funcao que trata o corpo de um html para remover os indicadores de tamanho dos chunks
def trata_corpo(body):

    #incializa a variavel que ira guardar o html modificado
    true_body = ""

    #se o corpo passado nao eh vazio
    if body != "":
        #para cada pedaco do html separado por quebra de linha 
        for i in body.split("\r\n"):
            #verifica se o item eh um numero hexadecimal
            try:
                int(i, 16)
            #se nao for adiciona ao corpo
            except:
                true_body += i
    #retorna o body modificado 
    return true_body