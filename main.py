import socket
import sys
import codecs
from request_maker import busca_html_base, busca_img
from parsing import get_src_img, tratar_url

#funcao que cria o socket, conecta no host e o retorna 
def estabelece_conexao(socket_tuple):

    #cria um socket tcp
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    #tenta conectar ao socket
    try:
        client.connect(socket_tuple)
    #se ocorrer uma excecao pede para o usuario digitar um url valido e termina o programa
    except:
        print("Digite um URL valido")
        sys.exit(1)
    #se deu tudo certo retorna o socket
    return client

#funcao que cria o arquivo html
def cria_arquivo_html(nome_do_arquivo, dados):

    #abre o arquivo com a codificacao utf-8
    arquivo = codecs.open(nome_do_arquivo, 'w', "utf-8")
    #escreve o html no arquivo
    arquivo.write(dados)
    #fecha o arquivo
    arquivo.close()

#funcao que cria o arquivo das imagens
def cria_arquivo_img(nome_do_arquivo, dados):

    #abre o arquivo
    arquivo = open(nome_do_arquivo, 'w')
    #escreve os dados da imagem
    arquivo.write(dados)
    #fecha o arquivo
    arquivo.close()

#inicio da parte principal do programa

#obtem o host alvo e o possivel caminho a partir da url passada como argumento
host_alvo, caminho = tratar_url(sys.argv[1])
#porta da conexao eh 80 por padrao
porta_alvo = 80

#cria o socket
cliente = estabelece_conexao((host_alvo, porta_alvo))

#busca o html base
body = busca_html_base(cliente, host_alvo, caminho)

#fecha a conexao e reestabelece
cliente.close()
cliente = estabelece_conexao((host_alvo, porta_alvo))

#cria o arquivo index.html
cria_arquivo_html("index.html", body)

#obtem os caminhos das imagens do html
img_src_urls = get_src_img(body)

#para cada imagem no vetor de caminho
for img in img_src_urls:

    #busca a imagem
    imagem = busca_img(cliente, host_alvo, img, caminho)
    #se a busca foi feita com sucesso
    if imagem != "-1":
        #cria o arquivo da imagem
        cria_arquivo_img(img.split("/")[-1], imagem)

    #refaz a conexao
    cliente.close()
    cliente = estabelece_conexao((host_alvo, porta_alvo))

#encerra o socket
cliente.close()