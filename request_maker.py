import socket
import sys
import time
from parsing import tratar_url, trata_corpo
from bs4 import BeautifulSoup

#funcao responsavel por iniciar a requisicao do html base e iniciar o tratamento da resposta
#recebe um socket, o host alvo e um possivel caminho do html
def busca_html_base(client, host_alvo, caminho):

    #verifica se o html esta em um caminho do host ou se eh a pagina principal do host

    if caminho != None:

        #se existir um caminho para a pagina, monta a requisicao HTTP
        #Accept-Encoding: identity para receber as paginas em um fluxo de bytes sempre que possivel
        #Connection: Close para que possamos saber que o servidor terminou de enviar a resposta
        requisicao = "GET /{} HTTP/1.1\r\nHost:{}\r\nAccept-Encoding: identity\r\nConnection: Close\r\n\r\n".format(caminho, host_alvo)

    else:

        #faz o mesmo processo descrito acima para caso a pagina requisitada nao estar em um caminho no host
        requisicao = "GET / HTTP/1.1\r\nHost:{}\r\nAccept-Encoding: identity\r\nConnection: Close\r\n\r\n".format(host_alvo)

    #envia a requisicao atraves do socket recebido
    client.send(requisicao.encode())

    #chama a funcao que faz o tratamento da resposta
    header, body, status_request = get_response(client, False)

    #se a requisicao nao foi bem sucedida, envia uma mensagem de erro e sai
    if status_request != 200:
        print("Nao foi possivel obter o html. Status: {}".format(status_request))
        sys.exit(1)

    #retorna o html recebido
    return body

#funcao responsavel por iniciar a requisicao de uma imagem e iniciar o tratamento da resposta
#recebe um socket, o host alvo , a url da imagem(se existir) e um possivel caminho
def busca_img(client, host_alvo, img_url, caminho):
    
    #verifica se o caminho da imagem eh uma url completa ou se e um caminho no host do html e trata de acordo
    #se o caminho existe, mas a palavra index esta no caminho, estou assumindo que a url ja esta com algum tipo de 
    #encaminhamento e ignoro o caminho, como eh o caso de http://www.ic.uff.br/index.php/pt/
    if "http:" in img_url or caminho == None or "index" in caminho:
        #verifica se o host que da url da imagem eh igual ao host do html e faz os tratamentos necessarios
        requisicao = "GET {} HTTP/1.1\r\nHost:{}\r\nConnection: Close\r\n\r\n".format(img_url, host_alvo)
    else:
        requisicao = "GET /{} HTTP/1.1\r\nHost:{}\r\nConnection: Close\r\n\r\n".format(caminho+ img_url, host_alvo)
    
    #envia a requisicao
    client.send(requisicao.encode())
    #chama a funcao que ira tratar o recebimento da resposta
    header, body, status_request = get_response(client, True)
    #se a resposta retornou um status de erro
    if status_request != 200:
        #printa uma mensagem de erro e retorna -1 para indicar que houve erro
        if "http:" in img_url:
            print("Nao foi possivel obter a imagem em /{}, Status: {}".format(img_url, status_request))
        else:
            print("Nao foi possivel obter a imagem em /{}, Status: {}".format(caminho+img_url, status_request))
        return "-1"
    #retorna o corpo da imagem
    return body

#funcao que trata o recebimento da resposta
def get_response(client, is_img):

    #recebe o primeiro pedaco da resposta
    chunk = client.recv(4096)
    #adiciona esse pedaco na variavel da resposta
    response = chunk
    #enquanto o servidor nao fechar a conexao
    while chunk:
        #recebe o proximo pedaco
        chunk = client.recv(4096)
        #adiciona na resposta
        response += chunk

    #se a funcao foi invocada para tratar de um html
    if not is_img:
        #faz uma decodificacao dos bytes html para uma string utf-8
        #se nao funcionar tenta com o latin-1
        try:
            str_response = response.decode("utf-8")
        except:
            try:
                str_response = response.decode("latin-1")
            except:
                print("codificacao invalida")
                sys.exit(1)
    else:
        #se a funcao foi invocada para tratar uma imagem, transforma os bytes em string
        str_response = str(response)

    body = ""

    #separa a resposta no primeiro \r\n\r\n entre header e body
    #primeiramente verificando se foi enviado um body na resposta
    parsed_response = str_response.split("\r\n\r\n", 1)
    if len(parsed_response) > 1:
        header = str_response.split("\r\n\r\n", 1)[0]
        body = str_response.split("\r\n\r\n", 1)[1]
    else:
        header = str_response.split("\r\n\r\n", 1)[0]
    
    #faz o tratamendo do body, para o caso do envio ter sido por chunks
    if not is_img:
        body = trata_corpo(body)
            
    #separa as linhas do header
    linhas_header = header.split("\r\n")
    #obtem o status_request da primeira linha do header
    status_request = int(linhas_header[0].split(" ")[1])
    
    #retorna os objetos tratados
    return header, body, status_request
