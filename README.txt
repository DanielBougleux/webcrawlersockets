Grupo:
Daniel Marcondes Bougleux Sodré
José Victor de Paiva e Silva

->para executar o programa é necessário ter o python 2 instalado na máquina
máquinas linux ja vem com o python instalado.

->também é necessário instalar o beautiful soup para o python 2. Isso pode ser
feito no terminal através do comando:

sudo apt-get install python-bs4

ou pelo pip do python 2 através do comando:

pip install beautifulsoup4

Para executar, entre pelo terminal na pasta onde o programa foi baixado e digite o seguinte comando:

python main.py [url do site]

OBS:

-> A princípio o programa suporta receber urls de sites que suportam apenas Transfer-Encoding: chunked,
isso foi testado na url http://www.ic.uff.br/index.php/pt/, que usa Transfer-Enconding: chunked
-> Apesar disso, enviamos o Accept-Encoding: identity no cabeçalho da requisição para evitar
o envio chunked se possível.